package createparty
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import org.openqa.selenium.Keys as Keys

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty


import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import java.text.*



class test {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Acess link")
	def Acesslink(){
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://dcadmin.yomabank.org:9044/admin-guiwar/login.html?reason=sessionEnded#/')
		WebUI.waitForPageLoad(10)
		WebUI.maximizeWindow()
	}

	@When("Enter Ycode (.*) and password (.*)")
	def EnterYcode(String Ycode,String password) {
		WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Admin Login/input_Login ID_login-loginid-input'), 10)
		WebUI.setText(findTestObject('Object Repository/Page_Admin Login/input_Login ID_login-loginid-input'), Ycode)
		//WebUI.setEncryptedText(findTestObject('Object Repository/Page_Admin Login/input_Password_login-password-input'), password)
		WebUI.setText(findTestObject('Object Repository/Page_Admin Login/input_Password_login-password-input'), password)
		Thread.sleep(2000)
	}

	@And("Click Sign In")
	def ClickSignIn(){
		WebUI.click(findTestObject('Object Repository/Page_Admin Login/button_Log In'))
	}

	@And("Click Client Tab")
	def ClickClientTab(){
		WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Admin/a_Clients_Update'), 20)
		WebUI.click(findTestObject('Object Repository/Page_Admin/a_Clients_Update'))
	}

	@And("Click Customer Management Tab")
	def ClickCustomerManagementTab(){
		WebUI.click(findTestObject('Object Repository/Page_Admin/a_Customer Management'))
	}

	@And("Enter (.*) CIF")
	def EnterCIF(String CIF){
		Thread.sleep(2000)
		WebUI.setText(findTestObject('Object Repository/Page_Admin/input_CIF ID_input'), CIF)
	}

	@And("Click Search button")
	def ClickSearchbutton(){
		WebUI.click(findTestObject('Object Repository/Page_Admin/button_Search'))
	}

	@And("Click View Details button (.*) and (.*)")
	def ClickViewDetailsbutton(String CIF, String Phone){
		Thread.sleep(2000)
		String ErrorMessage = WebUI.getText(findTestObject('Object Repository/Page_Admin/errormsg'))
		if(ErrorMessage == "This client only exists in the backend system. View the client details to replicate in Digital Channels.") {
			KeywordUtil.logInfo("Error message: "+ ErrorMessage)
		}
		else {
			WebUI.sendKeys(findTestObject('Object Repository/Page_Admin/button_View Details'), Keys.chord(Keys.ARROW_DOWN,Keys.ARROW_DOWN))
			WebUI.click(findTestObject('Object Repository/Page_Admin/button_View Details'))
			//Tick General check box
			WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Admin/div_ContractLastSaved'), 10)
			WebUI.click(findTestObject('Object Repository/Page_Admin/a_Client Details'))
			WebUI.click(findTestObject('Object Repository/Page_Admin/GENERAL'))
			WebUI.click(findTestObject('Object Repository/Page_Admin/check_GENERAL'))
			//Click Relations with Users
			WebUI.click(findTestObject('Object Repository/Page_Admin/a_Relations'))
			WebUI.click(findTestObject('Object Repository/Page_Admin/Owner_Relation'))
			WebUI.click(findTestObject('Object Repository/Page_Admin/IBN'))
			WebUI.click(findTestObject('Object Repository/Page_Admin/IBN ON OFF'))
			WebUI.click(findTestObject('Object Repository/Page_Admin/Relation_MBL'))
			WebUI.click(findTestObject('Object Repository/Page_Admin/MBL ON OFF'))
			//Click Signature Rule
			WebUI.click(findTestObject('Object Repository/Page_Admin/a_Signatures'))
			WebUI.click(findTestObject('Object Repository/Page_Admin/td_Signature Rule'))
			WebUI.click(findTestObject('Object Repository/Page_Admin/td_DefaultRule'))
			WebUI.click(findTestObject('Object Repository/Page_Admin/td_DefaultType'))
			WebUI.click(findTestObject('Page_Admin/div_Limit'))
			WebUI.setText(findTestObject('Object Repository/Page_Admin/input_Limit'), '99999999999999999')
			WebUI.click(findTestObject('Object Repository/Page_Admin/div_Currency'))
			WebUI.selectOptionByValue(findTestObject('Page_Admin/select_MMK'), 'string:MMK', true)
			WebUI.click(findTestObject('Object Repository/Page_Admin/button_Save'))
			//Reset Password
			WebUI.click(findTestObject('Object Repository/Page_Admin/a_Login ID and Devices'))
			WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Admin/click_IBN'), 5)
			WebUI.click(findTestObject('Object Repository/Page_Admin/click_IBN'))
			WebUI.click(findTestObject('Page_Admin/label_Username'))
			WebUI.setText(findTestObject('Page_Admin/input_Username_alias'), Phone)
			WebUI.click(findTestObject('Object Repository/Page_Admin/button_Reset Password_updated'))
			WebUI.sendKeys(findTestObject('Object Repository/Page_Admin/button_Apply Changes'), Keys.chord(Keys.UP,Keys.UP,Keys.UP))
			Thread.sleep(2000)
			WebUI.click(findTestObject('Object Repository/Page_Admin/button_Apply Changes'))
			WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Admin/button_Confirm'), 3)
			WebUI.click(findTestObject('Object Repository/Page_Admin/button_Confirm'))
			WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Admin/input_Comment_input12'), 5)
			WebUI.setText(findTestObject('Object Repository/Page_Admin/input_Comment_input12'), 'OK')
			Thread.sleep(2000)
			WebUI.click(findTestObject('Object Repository/Page_Admin/button_Accept'))
			Thread.sleep(2000)
			String SuccessMessage = WebUI.getText(findTestObject('Object Repository/Page_Admin/div_        Close        Changes successfully saved and applied'))
			WebUI.verifyEqual(SuccessMessage, "Changes successfully saved and applied.")
			println("CIF "+CIF+ SuccessMessage)
			WebUI.closeBrowser()
		}
	}
}


