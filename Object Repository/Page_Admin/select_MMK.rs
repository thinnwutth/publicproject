<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_MMK</name>
   <tag></tag>
   <elementGuidId>9bdf9e60-e518-4e8d-b148-a8aed65b1e12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select.col-md-6.ng-untouched.ng-valid.ng-not-empty.ng-dirty.ng-valid-parse</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Currency :'])[1]/following::select[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-md-6 ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-parse</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>vm.combination.currency</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-options</name>
      <type>Main</type>
      <value>currency.text as (currency.text) for currency in vm.currencies</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>AFNALLANGARSAUDAWGBAMBBDBGNBHDBMDBNDBOBBRLBSDBWPBYRBZDCADCHFCLPCNYCOPCRCCUPCZKDKKDOPEEKEGPEURFJDFKPGBPGGPGHCGIPGTQGYDHKDHNLHRKHUFIDRILSIMPINRIRRISKJEPJMDJPYKGSKHRKPWKRWKYDKZTLAKLBPLKRLRDLTLLVLMMKMNTMURMXNMYRMZNNADNGNNIONOKNPRNZDOMRPABPENPHPPKRPLNPYGQARRONSARSBDSCRSEKSGDSHPSOSSRDSVCSYPTHBTRLTRYTTDTVDTWDUAHUSDUYUUZSVEFVNDXCDYERZARZWD</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Currency :'])[1]/following::select[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Currency :'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Limit :'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[3]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/select</value>
   </webElementXpaths>
</WebElementEntity>
