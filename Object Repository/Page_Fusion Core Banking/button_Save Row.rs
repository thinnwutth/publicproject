<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Save Row</name>
   <tag></tag>
   <elementGuidId>c3d00271-22a4-4593-aaff-a2b8302e7270</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@type = 'button' and (text() = 'Save Row' or . = 'Save Row')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and (text() = 'Save Row' or . = 'Save Row')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>saveRow_HiddenVector_SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls011748deaa8d0hhy%1481660</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>iconclass</name>
      <type>Main</type>
      <value>dijitMisysIcon miconsave</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>require([&quot;bf/controls&quot;], function(controls){controls.saveRow({&quot;gridControlId&quot;:&quot;_HiddenVector_SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls011748deaa8d0hhy%1481660&quot;,&quot;callService&quot;:{&quot;srcFormId&quot;:&quot;011748deaa8d0hhy%1481660&quot;,&quot;requestMapping&quot;:{&quot;19ToDate&quot;:&quot;[\&quot;_HiddenVector_SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls011748deaa8d0hhy%1481660\&quot;]&quot;,&quot;19docID&quot;:&quot;[\&quot;_HiddenVector_SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls011748deaa8d0hhy%1481660\&quot;]&quot;,&quot;03DocumentType&quot;:&quot;[\&quot;SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls:DocumentData:documentDtl:documentType011748deaa8d0hhy%1481660\&quot;]&quot;,&quot;19SearchPartyDocumentData:DocumentDataDtls&quot;:&quot;[\&quot;_HiddenVector_SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls011748deaa8d0hhy%1481660\&quot;]&quot;,&quot;04KYCExpiryDate&quot;:&quot;[\&quot;SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls:DocumentData:KYCExpiryDate011748deaa8d0hhy%1481660\&quot;]&quot;,&quot;03DocumentCategory&quot;:&quot;[\&quot;SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls:DocumentData:documentDtl:documentCategory011748deaa8d0hhy%1481660\&quot;]&quot;,&quot;03ImageID&quot;:&quot;[\&quot;SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls:DocumentData:imageID011748deaa8d0hhy%1481660\&quot;]&quot;,&quot;19FromDate&quot;:&quot;[\&quot;_HiddenVector_SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls011748deaa8d0hhy%1481660\&quot;]&quot;,&quot;03SearchPartyDocumentData:extensionDetails&quot;:&quot;[\&quot;OffScreen_SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:extensionDetails011748deaa8d0hhy%1481660\&quot;]&quot;},&quot;eventSource&quot;:&quot;uxp&quot;,&quot;serviceDisplayName&quot;:&quot;PT_PFN_WrapperForValidatingDocumentData_PRC&quot;,&quot;serviceName&quot;:&quot;PT_PFN_WrapperForValidatingDocumentData_PRC&quot;}});});</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Save Row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;saveRow_HiddenVector_SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls011748deaa8d0hhy%1481660&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='saveRow_HiddenVector_SearchPartyDocumentDataRs:SearchPartyDocumentDataOutput:DocumentDataDtls011748deaa8d0hhy%1481660']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='011748deaa8d0hhy%1481660']/table/tbody/tr[24]/td[2]/div/div[3]/button[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove Row'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Row'])[1]/following::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Document Category'])[2]/preceding::button[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Document Type'])[2]/preceding::button[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Save Row']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[3]</value>
   </webElementXpaths>
</WebElementEntity>
